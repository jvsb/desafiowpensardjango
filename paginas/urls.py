from django.urls import path
from .views import IndexView


urlpatterns = [#Serve para colocar os caminhos que o usuário terá que colocar para chegar em determinado site.
    path('',IndexView.as_view(), name=('index')), #Primeira tag seria o diretorio da pagina, a segunda o conteudo que vai aparecer e a terceira seria um nome para esse diretorio
]