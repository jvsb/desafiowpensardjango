from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import views as auth_views

class TesteUrls(TestCase):

    #TESTANDO URLS Usuarios
    def test_url_Login(self):
        url = reverse('Login')
        self.assertEquals(resolve(url).func.view_class,auth_views.LoginView)
    
    def test_url_Logout(self):
        url = reverse('Logout')
        self.assertEquals(resolve(url).func.view_class,auth_views.LogoutView)
    

class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.login_url=reverse('Login')
        self.logout_url=reverse('Logout')
    
    def test_login_GET(self):
        response = self.client.get(self.login_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/login.html')
    
    def test_logout_GET(self):
        response = self.client.get(self.logout_url)

        self.assertEquals(response.status_code, 302)
