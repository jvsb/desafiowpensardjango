from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from cadastros.models import Compra, Produto
from cadastros.views import CompraCreate, CompraUpdate, CompraDelete, CompraList, ProdutoCreate, ProdutoUpdate, ProdutoDelete, ProdutoList



class TesteUrls(TestCase):

    def setUp(self):
        self.client = Client()

    #TESTANDO URLS PRODUTOS
    def test_url_CreateProduto(self):
        url = reverse('Cadastro Produto')
        self.assertEquals(resolve(url).func.view_class, ProdutoCreate)
    def test_url_UpdateProduto(self):
        url = reverse('Editar Produto', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, ProdutoUpdate)
    def test_url_DeleteProduto(self):
        url = reverse('Excluir Produto', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, ProdutoDelete)
    def test_url_ListProduto(self):
        url = reverse('Listar Produto')
        self.assertEquals(resolve(url).func.view_class, ProdutoList)
    
    #TESTANDO URLS COMPRAS
    def test_url_CreateCompra(self):
        url = reverse('Cadastro Compra')
        self.assertEquals(resolve(url).func.view_class, CompraCreate)
    def test_url_UpdateCompra(self):
        url = reverse('Editar Compra', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, CompraUpdate)
    def test_url_DeleteCompra(self):
        url = reverse('Excluir Compra', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.view_class, CompraDelete)
    def test_url_ListCompra(self):
        url = reverse('Listar Compra')
        self.assertEquals(resolve(url).func.view_class, CompraList)
    

class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get_or_create(username='testuser')[0], backend=None)
        self.produto1 = Produto.objects.create(
            Nome = 'Frango'
        )
        self.compra1 = Compra.objects.create(
            Valor = 500,
            produto = self.produto1,
            Quantidade = 20
        )

        self.lista_produtos =  reverse('Listar Produto')
        self.deleta_produtos =  reverse('Excluir Produto', kwargs={'pk': self.produto1.pk})
        self.edita_produtos = reverse('Editar Produto', kwargs={'pk': self.produto1.pk})
        self.cadastra_produtos =  reverse('Cadastro Produto')
        self.lista_compras =  reverse('Listar Compra')
        self.deleta_compras =  reverse('Excluir Compra', kwargs={'pk': self.compra1.pk})
        self.edita_compras = reverse('Editar Compra', kwargs={'pk': self.compra1.pk})
        self.cadastra_compras =  reverse('Cadastro Compra')


    def test_list_produtos_GET(self):
        response = self.client.get(self.lista_produtos)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/lista-produto.html')
    
    def test_cadastro_produtos_GET(self):
        response = self.client.get(self.cadastra_produtos)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/form-produto.html')  
   
    def test_edita_produtos_GET(self):
        response = self.client.get(self.edita_produtos)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/form.html')
    
    def test_deleta_produtos_GET(self):
        response = self.client.get(self.deleta_produtos)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/form-delete.html')




    def test_list_compras_GET(self):
        response = self.client.get(self.lista_compras)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/lista-compras.html')
    
    def test_cadastro_compras_GET(self):
        response = self.client.get(self.cadastra_compras)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/form.html')  
   
    def test_edita_compras_GET(self):
        response = self.client.get(self.edita_compras)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/form.html')
    
    def test_deleta_compras_GET(self):
        response = self.client.get(self.deleta_compras)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'cadastros/form-delete.html')


class TestModelProduto(TestCase):
    
    def setUp(self):
        self.produto = Produto.objects.create(Nome= 'abacate')
    
    def test_str_produto_return_name(self):
        test = 'abacate'
        result = str(self.produto)
        self.assertEquals(test, result)


class TestModelCompra(TestCase):

    def setUp(self):
        self.produto = Produto.objects.create(Nome= 'abacate')
        s = self.produto.id
        self.compra = Compra.objects.create(
            produto_id = self.produto.id,
            Valor = 200,
            Quantidade = 50,
        )

    def test_str_compra_return_produto(self):
        test = 'abacate, (200), (50)'
        result = str(self.compra)
        self.assertEquals(test, result)

    def test_compra_precoMedio(self):
        test = 4
        result = self.compra.precoMedio
        self.assertEquals(test, result)
