from django.db import models
from django.db.models import Sum, Avg
from django.db.models.fields import DecimalField, IntegerField
from django.db.models.fields.related import ForeignKey
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.exceptions import ValidationError
import itertools

# Create your models here.
def validate_not_spaces(value):
    if isinstance(value, str) and value.strip() == ' ':
        raise ValidationError(u"Você está tentando um valor em branco")


class Produto(models.Model):
    Nome = models.CharField(unique=True, blank=False, null=False, max_length=50, verbose_name="Nome Produto", validators=[validate_not_spaces])
    
    
    def quantidadeTotal(self):
        c = Compra.objects.values_list('produto').annotate(quantidadeSum = Sum('Quantidade'))
        c = list(itertools.chain(*c))
        d = 0
        s = self.id
        length = len(c)-1
        for x in range(length):
            if s == c[x]:
                d = c[(x+1)]

        return "{}".format(d)

    
    def precoMedioTotal(self,format=None):
        v = Compra.objects.values_list('produto').annotate(somaValor = Sum('Valor', output_field=DecimalField(decimal_places=2)))
        v = list(itertools.chain(*v))
        q = Compra.objects.values_list('produto').annotate(somaValor = Sum('Quantidade', output_field=DecimalField(decimal_places=2)))
        q = list(itertools.chain(*q))
        s = self.pk
        new = []
        vetorVazio = []
        length2 = len(v)
        for y in range(length2):
            if y % 2 == 0:
                new.append((v[y]))
            if y % 2 == 1:
                if q[y]>0:
                    new.append((v[y])/(q[y]))
                else:
                    new.append(0)
        
        length = len(new)-1
        if new == vetorVazio:
            d = 0
            return d
        else:
            for x in range(length):
                if s == new[x]:
                    d = new[(x+1)]
                    return "{}".format(round(d, 2))
                else:
                    d = 0
        return "{}".format(round(d, 2))
    
    def __str__(self):
        return "{}".format(self.Nome)

class Compra(models.Model):
    produto = ForeignKey(Produto, on_delete=models.CASCADE)
    Valor = models.DecimalField(blank=False,null=False, max_digits=7, decimal_places=2, default=0, validators=[MinValueValidator(0), MaxValueValidator(100000)], verbose_name="Preço Compra")
    Quantidade = models.PositiveIntegerField(blank=False, null=False, default=0 , validators=[MinValueValidator(1), MaxValueValidator(1000)], verbose_name="Quantidade Compra")
    PrecoMedio = models.FloatField(default=0, verbose_name="Preco Médio")
    
    @property
    def precoMedio(self):
        if self.Quantidade>0:
            c = (self.Valor/self.Quantidade)
            c = round(c, 2)
        else:
            c = 0
        return c

    def save(self, *args, **kwargs):
        self.PrecoMedio = self.precoMedio
        super(Compra, self).save(*args, **kwargs)

    

    def __str__(self):
        return "{}, ({}), ({})".format(self.produto, self.Valor, self.Quantidade)
    