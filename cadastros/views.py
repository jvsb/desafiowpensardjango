from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from .models import Produto, Compra
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin


class ProdutoCreate(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('Login') 
    model = Produto #model seria o modelo que vc vai cadastrar/atualizar
    fields = ['Nome'] #campos a ser preenchidos
    template_name = 'cadastros/form-produto.html'
    success_url = reverse_lazy('Listar Produto')

class CompraCreate(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('Login') 
    model = Compra
    fields = ['produto', 'Valor', 'Quantidade']
    template_name = 'cadastros/form.html'
    success_url = reverse_lazy('Listar Compra')

class ProdutoUpdate(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('Login') 
    model = Produto
    fields = ['Nome']
    template_name = 'cadastros/form.html'
    success_url = reverse_lazy('Listar Produto')

class CompraUpdate(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('Login') 
    model = Compra
    fields = ['produto', 'Valor', 'Quantidade']
    template_name = 'cadastros/form.html'
    success_url = reverse_lazy('Listar Compra')

class ProdutoDelete(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy('Login') 
    model = Produto
    template_name = 'cadastros/form-delete.html'
    success_url = reverse_lazy('Listar Produto')

class CompraDelete(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy('Login') 
    model = Compra
    template_name = 'cadastros/form-delete.html'
    success_url = reverse_lazy('Listar Compra')

class ProdutoList(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('Login') 
    model = Produto
    fields = ['Nome']
    template_name = 'cadastros/lista-produto.html'

class CompraList(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('Login') 
    model = Compra
    fields = ['produto', 'Valor', 'Quantidade']
    template_name = 'cadastros/lista-compras.html'