from django.urls import path
from .views import CompraCreate, CompraUpdate, CompraDelete, CompraList, ProdutoCreate, ProdutoUpdate, ProdutoDelete, ProdutoList

urlpatterns = [
    path('cadastro/produto/', ProdutoCreate.as_view(), name="Cadastro Produto"),
    path('cadastro/compra/', CompraCreate.as_view(), name="Cadastro Compra"),
    path('editar/produto/<pk>', ProdutoUpdate.as_view(), name="Editar Produto"),
    path('editar/compra/<pk>', CompraUpdate.as_view(), name="Editar Compra"),
    path('excluir/produto/<pk>', ProdutoDelete.as_view(), name="Excluir Produto"),
    path('excluir/compra/<pk>', CompraDelete.as_view(), name="Excluir Compra"),
    path('listar/produto/', ProdutoList.as_view(), name="Listar Produto"),
    path('listar/compra/', CompraList.as_view(), name="Listar Compra"),
    
]