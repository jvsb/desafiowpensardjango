# Generated by Django 3.2.8 on 2021-10-17 20:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cadastros', '0003_compra_quantidade'),
    ]

    operations = [
        migrations.AlterField(
            model_name='compra',
            name='produto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cadastros.produto'),
        ),
    ]
