# Generated by Django 3.2.8 on 2021-10-18 13:03

import cadastros.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cadastros', '0004_alter_compra_produto'),
    ]

    operations = [
        migrations.AddField(
            model_name='compra',
            name='precoMedio',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=5, verbose_name='Preco Médio Compras'),
        ),
    ]
