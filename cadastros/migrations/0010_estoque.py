# Generated by Django 3.2.8 on 2021-10-18 18:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cadastros', '0009_remove_produto_precomediototal'),
    ]

    operations = [
        migrations.CreateModel(
            name='Estoque',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('QuantidadeTotal', models.PositiveIntegerField(default=0, verbose_name='Quantidade Total')),
                ('PrecoMedioTotal', models.FloatField(default=0, verbose_name='Preço Medio Total')),
                ('produto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='cadastros.produto')),
            ],
        ),
    ]
